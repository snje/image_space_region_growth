#include <cmath>

#include <math.h>

#include <opencv2/opencv.hpp>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>


#include <smmintrin.h>

static float f_n_i[4];
static float f_n_j[4];

template <typename NormalType>
inline float normal_angle(NormalType n_i, NormalType n_j) {
    return n_i.normal_x * n_j.normal_x + n_i.normal_y * n_j.normal_y
            + n_i.normal_z * n_j.normal_z;
}

template <typename PointType>
inline float point_distance_sq(PointType p_i, PointType p_j) {
    float dx = p_i.x - p_j.x;
    float dy = p_i.y - p_j.y;
    float dz = p_i.z - p_j.z;
    //return dx * dx + dy * dy + dz * dz;
    return dz * dz;
}

template <typename T>
T fast_max(const T a, const T b) {
    return a > b ? a : b;
}


template <typename T>
T fast_min(const T a, const T b) {
    return a < b ? a : b;
}

template <typename PointType, typename NormalType>
int image_space_region_growth(
    typename pcl::PointCloud<PointType>::Ptr cloud,
    typename pcl::PointCloud<NormalType>::Ptr normal,
    const float normal_threshold, const float curvature_threshold,
    const int window, const float distance_threshold,
    std::vector<std::vector<int> > * const regions
) {
    regions->clear();
    float cos_normal_threshold = cos(normal_threshold);
    float distance_threshold_sq = distance_threshold * distance_threshold;
    // Structure for contain information regarding points
    // Allocate indicating which points are available
    bool allocated[normal->size()];
    std::vector<int> sorted_seeds;
    // Initilization
    //printf("rip\n");
    memset(allocated, 0, normal->size());
    for (int i = 0; i < normal->size(); i++) {
        PointType p = (*cloud)[i];
        if (std::isnan(p.x)) {
            allocated[i] = true;
        } else {
            allocated[i] = false;
            NormalType n = (*normal)[i];
            if (!std::isnan(n.curvature)) {
                sorted_seeds.push_back(i);
            }
        }
    }
    //printf("rip\n");
    // Sort according to curvature
    {
        struct Comparator {
            Comparator(typename pcl::PointCloud<NormalType>::Ptr n){
                this->normal = n;
            }
            bool operator() (int i, int j) {
                return (*this->normal)[i].curvature > (*this->normal)[j].curvature;
            }
            typename pcl::PointCloud<NormalType>::Ptr normal;
        };
        /*
        std::sort(
            sorted_seeds.begin(), sorted_seeds.end(),
            [normal](int i, int j) {
                return (*normal)[i].curvature > (*normal)[j].curvature;
            }
        );
        */
        std::sort(sorted_seeds.begin(), sorted_seeds.end(), Comparator(normal));
    }
    // The actual region growing
    int width = normal->width;
    int height = normal->height;
    //printf("rip\n");
    std::vector<int> active_region;
    std::vector<int> active_seeds;
    while (sorted_seeds.size()) {
        active_region.clear();
        active_seeds.clear();
        {
            int i = sorted_seeds[sorted_seeds.size() - 1];
            sorted_seeds.pop_back();
            if (allocated[i]) continue;
            active_seeds.push_back(i);
        }

        while (active_seeds.size()) {
            // Recover seed to grow from
            int i = active_seeds[active_seeds.size() - 1];
            active_seeds.pop_back();
            // Recover spatial 2D coordinates
            int i_y = i / width;
            int i_x = i % width;
            // Estimate alloted spread in image space
            int y_min = fast_max(0, i_y - window);
            int y_max = fast_min(i_y + window + 1, height);
            int x_min = fast_max(0, i_x - window);
            int x_max = fast_min(i_x + window + 1, width);
            // Multiply with width to get the effect memory offset
            y_min *= width;
            y_max *= width;
            // Recover and store the normal of the center point
            NormalType n_i = (*normal)[i];
            PointType p_i = (*cloud)[i];
            // Iterate through the neighboring region
            //printf("seed (%i, %i) (%i, %i) %i\n", i_x, i_y, width, height, i);
            //printf("Access x = (%i, %i) y = (%i, %i)\n", x_min, x_max, y_min, y_max);
            for (int y = y_min; y < y_max; y += width) {
                for (int x = x_min; x <= x_max; x++) {
                    int j = y + x;
            //        printf("access %i\n", j);
                    if (allocated[j]) continue;
                    NormalType n_j = (*normal)[j];
                    PointType p_j = (*cloud)[j];
                    float angle = normal_angle(n_i, n_j);
                    if (angle < cos_normal_threshold || std::isnan(angle)) continue;
                    float dist = point_distance_sq(p_i, p_j);
                    if (distance_threshold_sq < dist || std::isnan(dist)) continue;
                    active_region.push_back(j);
                    allocated[j] = true;
                    if (n_j.curvature < curvature_threshold) {
                        active_seeds.push_back(j);
                    }
                }
            }
        }
        regions->push_back(active_region);
    }
    return 0;
}


// Precompiled version of the above algorithmn
int image_space_region_growth_f32(
    pcl::PointCloud<pcl::Normal>::Ptr normal,
    const float normal_threshold, const float curvature_threshold,
    const int window, std::vector<std::vector<int> > * const regions
);
