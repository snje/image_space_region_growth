#include <math.h>

#include <pcl_conversions/pcl_conversions.h>
#include "segmentation_server.h"

SegmentationServer::SegmentationServer(ros::NodeHandle & nh) {
    setup_callbacks(nh);
}
//image_transport::Subscriber depth_sub = it.subscribe(
//    depth_topic, 1, depth_cb
//);
void SegmentationServer::setup_callbacks(ros::NodeHandle & nh) {
    this->cloud_listener = nh.subscribe(
        "/input_cloud", 10, &SegmentationServer::cloud_callback, this
    );
    this->segmentation_publisher = nh.advertise<sensor_msgs::PointCloud2>(
        "segmentation", 10
    );
}

void SegmentationServer::cloud_callback(
    const sensor_msgs::PointCloud2ConstPtr & cloud
) {
    printf("got the clouds\n");
    this->cloud_requests.push_back(cloud);
}

std::vector<pcl::PointCloud<PointType>::Ptr > SegmentationServer::get_requests() {
    std::vector<pcl::PointCloud<PointType>::Ptr > result;
    for (int i = 0; i < this->cloud_requests.size(); i++) {
        pcl::PointCloud<PointType>::Ptr cloud(new pcl::PointCloud<PointType>());
        pcl::fromROSMsg(*cloud_requests[i], *cloud);
        result.push_back(cloud);
    }
    return result;
}

void SegmentationServer::publish_segmentation(
    pcl::PointCloud<PointType>::Ptr cloud_ptr
) {
    sensor_msgs::PointCloud2 cloud_msgs;
    pcl::toROSMsg(*cloud_ptr,   cloud_msgs);
    this->segmentation_publisher.publish(cloud_msgs);
}

void SegmentationServer::clear() {
    this->cloud_requests.clear();
}

SegmentationParam::SegmentationParam(ros::NodeHandle & nh) {
    nh.param<float>("seg/angle", this->angle, 5.0f);
    nh.param<float>("seg/curvature", this->curvature, 15.0f);
    nh.param<int>("seg/window", this->window, 1);
    nh.param<float>("seg/distance", this->distance, 5.0f);

    nh.param<int>("seg/normal_rect", this->normal_rect, 5);
    nh.param<float>("seg/max_depth", this->max_depth, 0.02f);
    nh.param<float>("seg/normal_smooth", this->normal_smooth, 10.0f);

    this->distance *= 0.001;
    this->angle *= M_PI / 180;
}
