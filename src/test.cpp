#include <math.h>
#include <stdio.h>
#include <pcl/io/pcd_io.h>
#include <pcl/features/integral_image_normal.h>
#include <pcl/console/time.h>
#include <opencv2/opencv.hpp>

#include "algorithm.h"

typedef pcl::PointXYZRGB PointType;
typedef pcl::Normal NormalType;

int main(int argc, char ** argv) {
    if (argc < 2) {
        printf("Please supply path to input PCD file\n");
        return -1;
    }
    pcl::PointCloud<PointType>::Ptr cloud (new pcl::PointCloud<PointType>);
    pcl::PointCloud<NormalType>::Ptr normal (new pcl::PointCloud<NormalType>);

    if (pcl::io::loadPCDFile <PointType> (argv[1], *cloud) == -1) {
        printf("Failed to load file: %s\n", argv[1]);
        return -2;
    }
    pcl::IntegralImageNormalEstimation<PointType, NormalType> ne;
    ne.setNormalEstimationMethod (ne.COVARIANCE_MATRIX);
    ne.setMaxDepthChangeFactor(0.02f);
    ne.setNormalSmoothingSize(10.0f);
    ne.setInputCloud(cloud);
    ne.compute(*normal);

    std::vector<std::vector<int> > regions;
    pcl::console::TicToc tt;
    printf("yo\n");
    tt.tic();
    int err = image_space_region_growth<PointType, NormalType>(
        cloud, normal, 3 / 180.0 * M_PI, 70, 5, 0.005, &regions
    );
    printf("yo %f\n", tt.toc());

    cv::Mat im(cloud->height, cloud->width, CV_8UC3);
    for (int r = 0; r < regions.size(); r++) {
        uint8_t red = rand() % 256;
        uint8_t green = rand() % 256;
        uint8_t blue = rand() % 256;
        auto reg = regions[r];
        for (int i = 0; i < reg.size(); i++) {
            int index = reg[i];
            im.data[index * 3 + 0] = red;
            im.data[index * 3 + 1] = green;
            im.data[index * 3 + 2] = blue;
        }
    }

    cv::imshow("Da shit", im);
    while (cv::waitKey(16) != 27) {}

    return 0;
}
