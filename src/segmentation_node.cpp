#include <stdio.h>
#include <ros/ros.h>
#include <algorithm>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/features/integral_image_normal.h>
#include <limits>
#include <math.h>
#include <algorithm>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "algorithm.h"
#include "segmentation_server.h"

typedef pcl::Normal NormalType;
typedef pcl::PointCloud<PointType> PointCloud;
typedef pcl::PointCloud<NormalType> NormalCloud;
typedef std::vector<int> Region;

// Simply select the largest region
Region select_segment(std::vector<Region> & regions, PointCloud::Ptr cloud) {
    auto cmp = [] (Region a, Region b) {
        return a.size() > b.size();
    };
    std::sort(regions.begin(), regions.end(), cmp);
    double min_z = INFINITY;
    int min_reg = -1;

    for (int i = 0; i < std::min((int)regions.size(), 5); i++) {
        Region r = regions[i];
        if (r.size() < 1000) continue;
        double z = 0;
        for (int index: r) {
            z += (*cloud)[index].z;
        }
        z /= r.size();
        if (z < min_z) {
            min_z = z;
            min_reg = i;
        }
    }
    return min_reg == -1 ? regions[0] : regions[min_reg];
    //std::sort(regions.begin(), regions.end(), cmp);

//    return regions[0];
}

/*
cv::Mat colormap_segmentation(
    const int width, const int height,
    std::vector<Regions> & regions
) {
    uint8_t * color_data = new uint8_t[height * width * 3];
    memset(color_data, 0, height * width * 3);

    for (auto r: regions) {
        uint8_t red = rand() % 255;
        uint8_t green = rand() % 255;
        uint8_t blue = rand() % 255;

        for (auto index: regions) {
            color_data[index * 3 + 0] = red;
            color_data[index * 3 + 1] = green;
            color_data[index * 3 + 2] = blue;
        }
    }
    // TODO: ensure that mat takes ownership of the pointer
    return cv::Mat(height, width, CV_8UC3, color_data);
}
*/
PointCloud::Ptr do_segmentation(
    PointCloud::Ptr cloud, SegmentationParam param
) {
    NormalCloud::Ptr normal (new NormalCloud);
    PointCloud::Ptr output (new PointCloud(*cloud));

    pcl::IntegralImageNormalEstimation<PointType, NormalType> ne;
    ne.setNormalEstimationMethod (ne.COVARIANCE_MATRIX);
    ne.setMaxDepthChangeFactor(param.max_depth);
    ne.setNormalSmoothingSize(param.normal_smooth);
    ne.setInputCloud(cloud);
    ne.setRectSize(param.normal_rect, param.normal_rect);
    ne.useSensorOriginAsViewPoint();
    ne.compute(*normal);

    cv::Mat curvature(normal->height, normal->width, CV_32F);

    for (int y = 0; y < normal->height; y++) {
        for (int x = 0; x < normal->width; x++) {
            curvature.at<float>(y, x) = (*normal)[x + y * normal->width].curvature;
        }
    }
    double min, max;
    cv::minMaxLoc(curvature, &min, &max);
    printf("min %f, max %f", min, max);
    curvature -= min;
    curvature.convertTo(curvature, CV_8UC1, 255.0 / (max - min));
    cv::imwrite("/tmp/cruv.png", curvature);


    std::vector<Region> regions;
    int err = image_space_region_growth<PointType, NormalType>(
        cloud, normal, param.angle, param.curvature, param.window,
        param.distance, &regions
    );
    Region min_reg = select_segment(regions, cloud);

    for (int i = 0; i < output->size(); i++) {
        PointType p = (*output)[i];
        p.x = NAN;
        p.y = NAN;
        p.z = NAN;
        (*output)[i] = p;
    }
    for (int i = 0; i < min_reg.size(); i++) {
        int index = min_reg[i];
        (*output)[index] = (*cloud)[index];
    }
    /*
    for (int i = 1; i < regions.size(); i++) {
        for (int index: regions[i]) {
            PointType p = (*output)[index];
            p.x = NAN;
            p.y = NAN;
            p.z = NAN;
            (*output)[index] = p;
        }
    }
    */

    return output;
}

int main(int argc, char ** argv) {
    ros::init(argc, argv, "segmentation_node");
    ros::NodeHandle nh;

    SegmentationServer service(nh);
    SegmentationParam param(nh);
    ros::Rate fps(60);
    while (ros::ok()) {
        auto requests = service.get_requests();
        service.clear();

        for (auto cloud: requests) {
            printf("segmentting\n");
            auto segmented_cloud = do_segmentation(cloud, param);
            printf("publishing\n");
            service.publish_segmentation(segmented_cloud);
        }

        ros::spinOnce();
        fps.sleep();
    }

    return 0;
}
