#ifndef __SEGMENTATION_SERVER_H__
#define __SEGMENTATION_SERVER_H__

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <vector>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/sample_consensus/model_types.h>

typedef pcl::PointXYZ PointType;

class SegmentationServer {
public:
    SegmentationServer(ros::NodeHandle & nh);

    void publish_segmentation(pcl::PointCloud<PointType>::Ptr cloud_ptr);
    std::vector<pcl::PointCloud<PointType>::Ptr> get_requests();
    void clear();
private:
    void setup_callbacks(ros::NodeHandle & nh);

    void cloud_callback(const sensor_msgs::PointCloud2ConstPtr & cloud);

    std::vector<sensor_msgs::PointCloud2ConstPtr> cloud_requests;
    ros::Subscriber cloud_listener;
    ros::Publisher segmentation_publisher;
};


struct SegmentationParam {
    SegmentationParam(ros::NodeHandle & nh);

    float angle;
    float curvature;
    float distance;
    int window;

    int normal_rect;
    float max_depth;
    float normal_smooth;
};

#endif
