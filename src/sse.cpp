#include <smmintrin.h>
#include <pcl/console/time.h>
#include <pcl/point_types.h>
#include <stdio.h>

inline float sse_dot_product(
    float x1, float y1, float z1, float x2, float y2, float z2
){
    float result[4];
    __m128 _sse_n_i = _mm_set_ps(x1, y1, z1, 0);
    __m128 _sse_n_j = _mm_set_ps(x2, y2, z2, 0);
    __m128 _sse_dp = _mm_dp_ps(_sse_n_i, _sse_n_j, 0b11101000);
    _mm_store_ss(result, _sse_dp);
    return result[0];
}

inline float dot_product(
    float x1, float y1, float z1, float x2, float y2, float z2
){
    return x1 * x2 + y1 * y2 + z1 * z2;
}

#define COUNT 10000
#define DIM 4
#define REPS 10000

int main(int argc, char ** argv) {
    float n_i[] = {1, 2, 3, 4};
    float n_j[COUNT * DIM];

    for (int i = 0; i < COUNT * DIM; i++) {
        n_j[i] = i;
    }

    float sum = 0;

    pcl::console::TicToc tt;
    tt.tic();
    for (int r = 0; r < REPS; r++) {
        for (int i = 0; i < COUNT * DIM; i += DIM) {
            for (int d = 0; d < DIM; d++) {
                sum += n_i[d] * n_j[i + d];
            }
        }
    }
    printf("scalar took %f %f\n", tt.toc(), sum);

    __m128 dp_ij[COUNT];
    __m128 _mm_n_i = _mm_load_ps(n_i);
    float res[4];
    sum = 0;
    tt.tic();
    for (int r = 0; r < REPS; r++) {
        for (int i = 0; i < COUNT; i++) {
            __m128 _mm_n_j = _mm_load_ps(n_j + i * DIM);
            dp_ij[i] = _mm_dp_ps(_mm_n_i, _mm_n_j, 0b11110000);
        }
        for (int i = 0; i < COUNT; i++) {
            _mm_store_ss(res, dp_ij[i]);
            sum += res[0];
        }
    }
    printf("sse took %f %f\n", tt.toc(), sum);

    return 0;
}
